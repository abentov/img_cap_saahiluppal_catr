import pandas as pd

from torch.utils.data import Dataset
import torchvision.transforms.functional as TF
import torchvision as tv

from PIL import Image
import numpy as np
import random
import os

from transformers import BertTokenizer
from configuration import Config

from .utils import nested_tensor_from_tensor_list, read_json

MAX_DIM = 299

# region transforms
def under_max(image):
    if image.mode != 'RGB':
        image = image.convert("RGB")

    shape = np.array(image.size, dtype=np.float)
    long_dim = max(shape)
    scale = MAX_DIM / long_dim

    new_shape = (shape * scale).astype(int)
    image = image.resize(new_shape)

    return image


class RandomRotation:
    def __init__(self, angles=[0, 90, 180, 270]):
        self.angles = angles

    def __call__(self, x):
        angle = random.choice(self.angles)
        return TF.rotate(x, angle, expand=True)


train_transform = tv.transforms.Compose([
    RandomRotation(),
    tv.transforms.Lambda(under_max),
    tv.transforms.ColorJitter(brightness=[0.5, 1.3], contrast=[
                              0.8, 1.5], saturation=[0.2, 1.5]),
    tv.transforms.RandomHorizontalFlip(),
    tv.transforms.ToTensor(),
    tv.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
])

val_transform = tv.transforms.Compose([
    tv.transforms.Lambda(under_max),
    tv.transforms.ToTensor(),
    tv.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])
# endregion transforms


class CocoCaption(Dataset):

    def __init__(self,
                 root: str = './coco/train2017',
                 ann: dict = {},
                 max_length: int = 128,
                 limit: int = -1,
                 transform=train_transform,
                 mode='training'
     ):

        super().__init__()

        self.root = root  # this is he image dir
        self.transform = transform
        self.annot = [(self._process(val['image_id']), val['caption'])
                      for val in ann['annotations']]

        # Albert
        # if mode == 'validation':
        #     self.annot = self.annot
        if mode == 'training':
            print(f'{mode} len of ann before join with flickr is {len(self.annot)}')  # Albert
            self.annot = self.annot[: limit]
            self.join_flickr_anns()  # Albert - join with flickr on training data

        print(f'{mode} len of ann is {len(self.annot)}')  # Albert

        self.tokenizer = BertTokenizer.from_pretrained(
            'bert-base-uncased', do_lower=True)
        self.max_length = max_length + 1

    def join_flickr_anns(self):

        # NOTE - flickr images should be in ./coco/train2017 - joined with them - do this in the .ipynb
        # file = os.path.join(self.root, 'flickr30k_images/results.csv')
        file = os.path.join('./coco', 'flickr30k_images/results.csv')  # Albert hard
        df = pd.read_csv(file, sep='|')
        df.rename(columns={o: o.strip() for o in df.columns}, inplace=True)

        df['image_name'] = df['image_name'].str.strip()
        df['comment'] = df['comment'].str.strip()

        self.annot.extend(zip(df['image_name'], df['comment']))

    @staticmethod  # Albert
    def _process(image_id):
        val = str(image_id).zfill(12)  # Albert - yep go py! fill ascii '0' to up to 12
        return val + '.jpg'

    def __len__(self):
        return len(self.annot)

    def __getitem__(self, idx):
        image_id, caption = self.annot[idx]
        image = Image.open(os.path.join(self.root, image_id))

        if self.transform:
            image = self.transform(image)
        image = nested_tensor_from_tensor_list(image.unsqueeze(0))

        # huggingface - encode_plus captions
        caption_encoded = self.tokenizer.encode_plus(
            caption, max_length=self.max_length,
            pad_to_max_length=True, return_attention_mask=True,
            return_token_type_ids=False, truncation=True
        )

        caption = np.array(caption_encoded['input_ids'])
        # this is transformers mask thing from the architecture
        cap_mask = (
            1 - np.array(caption_encoded['attention_mask'])).astype(bool)

        return image.tensors.squeeze(0), image.mask.squeeze(0), caption, cap_mask


def build_dataset(config: Config, mode: str = 'training'):

    """
    Albert - does image transforms, prepare images, encode_plus captions/ann for huggingface trans...
    a lot of the stuff learned from Jeremy Howard... on images
    """

    if mode not in ('training', 'validation'):
        raise NotImplementedError(f"{mode} not supported")

    pref = 'train' if mode == 'training' else 'val'
    trans = train_transform if mode == 'training' else val_transform

    dirr = os.path.join(config.dir, f'{pref}2017')
    file = os.path.join(
        config.dir, 'annotations', f'captions_{pref}2017.json')

    # CocoCaption is a/inherits torch Dataset
    data = CocoCaption(
        root=dirr,
        ann=read_json(file),
        max_length=config.max_position_embeddings,
        limit=config.limit,
        transform=trans,
        mode=mode
    )

    return data


